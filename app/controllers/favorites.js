export default Ember.Controller.extend({
    actions: {
        scrollToTop: function () {
            $('html, body').animate({scrollTop: 0},
                {
                    duration: 'slow'
                });
        }
    },
    searchResult: function () {
        var searchTerm = this.get('model.searchTerm');
        var regExp = new RegExp(searchTerm, 'i');
        var self = this;

        if (this.get('model.isFiltered') && searchTerm === '') {
            self.set('model.files', self.get('model.rawFiles'));
            self.set('model.folders', self.get('model.rawFolders'));
            this.set('model.isFiltered', false);
        } else if (searchTerm !== '') {
            this.store.find('favoritefile').then(function (favoriteFiles) {
                var idArray = [];
                favoriteFiles.forEach(function (favoriteFile) {
                    idArray.push(favoriteFile.get('file'));
                });

                self.set('model.files', from(self.get('model.rawFiles')).where("@0.contains($.i.toString()) > 0 && @1.test($.i)",[idArray, regExp]).orderBy("$.i").toArray());
                self.set('model.isFiltered', true);
            });
            this.store.find('favoritefolder').then(function (favoriteFolders) {
                var idArray = [];
                favoriteFolders.forEach(function (favoriteFolder) {
                    idArray.push(favoriteFolder.get('folder'));
                });

                self.set('model.folders', from(self.get('model.rawFolders')).where("@0.contains($.i.toString()) > 0 && @1.test($.dn)",[idArray, regExp]).orderBy("$.i").toArray());
                self.set('model.isFiltered', true);
            });
        }
    }.observes('model.searchTerm')
});
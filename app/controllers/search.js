export default Ember.ObjectController.extend({
    actions: {
        scrollToTop: function () {
            $('html, body').animate({scrollTop: 0},
                {
                    duration: 'slow'
                });
        }
    },
    searchResult: function () {
        var searchTerm = this.get('model.searchTerm');
        var regExpSearch = new RegExp(searchTerm, 'i');

        if (this.get('model.isFiltered') && searchTerm === '') {
            this.set('model.files', this.get('model.rawFiles'));
            this.set('model.isFiltered', false);
        } else if (searchTerm !== '') {
            this.set('model.files', from(this.get('model.rawFiles')).where("@.test($.i)", regExpSearch).orderBy("$.i").toArray());
            this.set('model.isFiltered', true);
        }
    }.observes('model.searchTerm')
});
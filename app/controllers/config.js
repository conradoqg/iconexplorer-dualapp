export default Ember.ObjectController.extend({
    actions: {
        save: function (config) {
            var self = this;
            config.save().then(function () {
                self.success("Configuration saved!");
            }, function (response) {
                self.error("Save failed: ", response);
            });
        },
        reset: function (config) {
            var self = this;
            this.set('size', 60);
            this.set('color', '000000');
            this.set('path', App.findBasePath());
            config.save().then(function () {
                self.success("Configuration saved!");
            }, function (response) {
                self.error("Save failed: ", response);
            });
        },
        updateSize: function (size, force) {
            if (force || this.get('size') !== size)
                this.set('size', size);
        },
        selectPath: function (selectedPath) {
            this.set('path', selectedPath);
        },
        recreateIndex: function () {
            var self = this;
            var el = $('.progress');
            el.show();
            window.generateData(this.get('path'), 'data', function (error) {
                $('.progress').hide();
                if (typeof error !== 'undefined' && !error) {
                    self.error("Cannot create index!");
                } else {
                    self.success("Index recreated, restart the application to see the effects!");
                }
            }, function (total, read) {
                var perc = Math.round((100 * read) / total);
                el.attr('aria-valuemax', 100);
                el.attr('aria-valuenow', perc);
                el.css('width', perc + '%');
                el.find('.sr-only').text(perc + '% Complete');
            });
        }
    },

    success: function (text) {
        $("#saveFileSuccessText")[0].innerText = text;
        $("#saveFileSuccess").show();
        window.setTimeout(function () {
            $("#saveFileSuccess").hide();
        }, 5000);
    },

    error: function (text, response) {
        $("#saveFileErrorText")[0].innerText = text + response.error;
        $("#saveFileError").show();
    },

    updateSliderSize: function () {
        $("#slider").slider({ value: this.get('size') });
    }.observes('size'),

    updateColorPicker: function () {
        $('#color-picker').next('div.input-group-btn').find('.color-preview.current-color').css('background-color', '#' + this.get('color'));
    }.observes('color')
});
export default Ember.ObjectController.extend({
    actions: {
        setFavorite: function () {
            var self = this;
            self.store.find('favoritefolder', { folder: this.get('id') }).then(function (favoriteFolders) {
                if (favoriteFolders.get('length') === 0) {
                    self.store.createRecord('favoritefolder', {
                        folder: self.get('id')
                    }).save().then(function () {
                        self.set('isFavorite', true);
                    });
                } else {
                    favoriteFolders.forEach(function (favoriteFolder) {
                        favoriteFolder.deleteRecord();
                        favoriteFolder.save().then(function () {
                            self.set('isFavorite', false);
                        });
                    });
                }
            });
        },
        scrollToTop: function () {
            $('html, body').animate({scrollTop: 0},
                {
                    duration: 'slow'
                });
        }
    },

    isFavorite: function () {
        var self = this;
        self.store.find('favoritefolder', { folder: this.get('id') }).then(function (favoriteFolder) {
            self.set('isFavorite', favoriteFolder.get('length') > 0);
        });
        return false;
    }.property()
});
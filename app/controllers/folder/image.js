export default Ember.ObjectController.extend({
    size: null,
    color: null,
    actions: {
        saveSVG: function (filename) {

            if (App.isNodeWebkit && !filename) return;

            if ($("#svg_image_id").length === 1) {
                var self = this;
                var svgHTML = $("#svg_image_id")[0].contentDocument.documentElement;

                if (App.isNodeWebkit) {
                    svgHTML = $('<div>').append($(svgHTML).clone()).html();
                } else {
                    svgHTML = svgHTML.outerHTML;
                }

                var imgsrc = 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent(svgHTML);
                var canvas = document.querySelector("canvas"),
                    context = canvas.getContext("2d");

                var image = new window.Image();
                image.src = imgsrc;
                image.onload = function () {
                    context.drawImage(image, 0, 0);
                    var canvasdata = canvas.toDataURL("image/png");

                    var size = Math.max(self.get('size'), 120);
                    var x = window.screen.width / 2 - size / 2;
                    var y = window.screen.height / 2 - size / 2;
                    if (App.isNodeWebkit) {
                        var fs = App.node_require('fs');
                        canvasdata = canvasdata.replace(/^data:image\/png;base64,/, "");
                        fs.writeFile(filename, canvasdata, 'base64', function (err) {
                            if (err) {
                                $("#saveFileErrorText")[0].innerText = "Write failed to: " + filename + ", error: " + err;
                                $("#saveFileError").show();
                            } else {
                                $("#saveFileSuccessText")[0].innerText = "File saved to: " + filename;
                                $("#saveFileSuccess").show();
                                window.setTimeout(function () {
                                    $("#saveFileSuccess").hide();
                                }, 5000);
                            }
                        });
                    } else {
                        window.open(canvasdata, 'save image', 'width=' + size + ',height=' + size + ',left=' + x + ',top=' + y);
                    }
                };
            }
        },

        updateSize: function (size, force) {
            if (force || this.get('size') !== size)
                this.set('size', size);
        },

        setFavorite: function () {
            var self = this;
            self.store.find('favoritefile', { file: this.get('file.i').toString() }).then(function (favoriteFiles) {
                if (favoriteFiles.get('length') === 0) {
                    self.store.createRecord('favoritefile', {
                        file: self.get('file.i').toString()
                    }).save().then(function () {
                        self.set('isFavorite', true);
                    });
                } else {
                    favoriteFiles.forEach(function (favoriteFile) {
                        favoriteFile.deleteRecord();
                        favoriteFile.save().then(function () {
                            self.set('isFavorite', false);
                        });
                    });
                }
            });
        }
    },

    changeSVGSize: function () {
        var self = this;
        var size = self.get('size');
        var el = $("#svg_image_id");
        if (el.length === 1) {
            var svgElement = el[0].contentDocument.documentElement;
            if (svgElement) {
                svgElement.setAttribute('width', size);
                svgElement.setAttribute('height', size);
            }
        }
    }.observes('size'),

    updateSliderSize: function () {
        $("#slider").slider({ value: this.get('size') });
    }.observes('size'),

    updateColorPicker: function () {
        $('#color-picker').next('div.input-group-btn').find('.color-preview.current-color').css('background-color', '#' + this.get('color'));
    }.observes('color'),

    changeSVGColor: function () {
        var self = this;
        var color = self.get('color');
        if ($("#svg_image_id").length === 1) {
            var svgElement = $("#svg_image_id")[0].contentDocument.documentElement;
            if (svgElement) {
                // Remove svgGrid el
                $(svgElement).find('#svgGrid').remove();

                // Lookout for fill attributes
                $(svgElement).attr('fill', function (index, value) {
                    return '#' + color;
                });

                // Lookout for fill tags
                $(svgElement).find('[fill]').attr('fill', function (index, value) {
                    if (value)
                        return '#' + color;
                    else
                        return value;
                });

                // Lookout for fill style
                $(svgElement).find('*').css('fill', function (index, value) {
                    if (value)
                        return '#' + color;
                    else
                        return value;
                });
            }
        }
    }.observes('color'),

    dontShowFavoritePopoverAnymore: function() {
        this.store.find('config','default').then(function(config) {
            config.set('showFavoriteIconPopover', false);
            config.save();
        });
    }
});
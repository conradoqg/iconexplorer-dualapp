import folders from 'appkit/models/folder';

export default Ember.ObjectController.extend({
    actions: {
        setFavorite: function () {
            var self = this;
            this.store.find('favoritefolder', { folder: this.get('folder.i').toString() }).then(function (favoriteFolders) {
                if (favoriteFolders.get('length') === 0) {
                    self.store.createRecord('favoritefolder', {
                        folder: self.get('folder.i').toString()
                    }).save().then(function () {
                        self.set('model.isFavorite', true);
                    });
                } else {
                    favoriteFolders.forEach(function (favoriteFolder) {
                        favoriteFolder.deleteRecord();
                        favoriteFolder.save().then(function () {
                            self.set('model.isFavorite', false);
                        });
                    });
                }
            });
        },
        scrollToTop: function () {
            $('html, body').animate({scrollTop: 0},
                {
                    duration: 'slow'
                });
        }
    },
    searchResult: function () {
        var searchTerm = this.get('model.searchTerm');
        var regExp = new RegExp(searchTerm, 'i');

        if (this.get('model.isFiltered') && searchTerm === '') {
            this.set('model.isFiltered', false);
            this.set('model.files', this.get('model.rawFiles'));
        } else if (searchTerm !== '') {
            this.set('model.isFiltered', true);
            this.set('model.files', from(this.get('model.rawFiles')).where("@.test($.i)", regExp).orderBy("$.i").toArray());
        }
    }.observes('model.searchTerm'),

    dontShowFavoritePopoverAnymore: function() {
        this.store.find('config','default').then(function(config) {
            config.set('showFavoritePackPopover', false);
            config.save();
        });
    }
});
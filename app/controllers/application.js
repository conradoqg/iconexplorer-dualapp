export default Ember.ObjectController.extend({
    search: '',

    actions: {
        search: function () {
            var query = this.get('search');
            this.transitionToRoute('search', query);
        },
        openCreatorsWebsite: function () {
            if (App.isNodeWebkit) {
                var gui = App.node_require('nw.gui');
                gui.Shell.openExternal('http://www.conradoqg.eti.br');
            }
        },
        openSourceCodeWebsite: function () {
            if (App.isNodeWebkit) {
                var gui = App.node_require('nw.gui');
                gui.Shell.openExternal('http://git.conradoqg.eti.br/conradoqg/iconexplorer-dualapp');
            }
        }
    }
});
import folders from 'appkit/models/folder';

export default Ember.ObjectController.extend({
    searchResult: function () {
        var searchTerm = this.get('model.searchTerm');
        var regExp = new RegExp(searchTerm, 'i');

        if (searchTerm === '') {
            this.set('model.folders', folders);
        } else {
            this.set('model.folders', from(folders).where("@.test($.dn)", regExp).orderBy("$.i").toArray());
        }
    }.observes('model.searchTerm')
});
import Resolver from 'ember/resolver';
import loadInitializers from 'ember/load-initializers';


window.App = Ember.Application.extend({
    modulePrefix: 'appkit', // TODO: loaded via config
    Resolver: Resolver,
    LOG_ACTIVE_GENERATION: false,
    LOG_MODULE_RESOLVER: false,
    LOG_TRANSITIONS: false,
    LOG_TRANSITIONS_INTERNAL: false,
    LOG_VIEW_LOOKUPS: false,
    MODEL_FACTORY_INJECTIONS: false,

    isNodeWebkit: (typeof process === "object"),
    saveWithRigthClick: bowser.msie,

    findBasePath: function () {
        var baseDir = '../images';

        if (App.isNodeWebkit) {
            var fs = App.node_require('fs');
            var path = App.node_require('path');

            var dirsToTry = [
                path.resolve(path.dirname(document.process.execPath) + path.sep, '..' + path.sep + 'images'),
                path.resolve(path.dirname(document.process.mainModule.filename) + path.sep, '..' + path.sep + 'images')
            ];

            dirsToTry.forEach(function (item) {
                if (fs.existsSync(item))
                    baseDir = item;
            });
        }
        return baseDir;
    },
    node_require: function (name) {
        if (window.nwDispatcher) {
            window.ember_require = window.require;
            window.require = window.node_require;
            var returnData = window.require(name);
            window.require = window.ember_require;
            return returnData;
        } else {
            throw new Error("Can't require node module when using non-node application");
        }
    }
});

loadInitializers(App, 'appkit');

export default App;
// Please note that Handlebars helpers will only be found automatically by the
// resolver if their name contains a dash (reverse-word, translate-text, etc.)
// For more details: http://stefanpenner.github.io/ember-app-kit/guides/using-modules.html

export default Ember.Handlebars.makeBoundHelper(function (badgeLevel) {
    var classs = "";
    var options = {
        low: 0,
        mid: 200,
        hight: 400
    };
    if (badgeLevel >= options.higth)
        classs = "alert-success";
    else if (badgeLevel >= options.mid)
        classs = "alert-warning";
    else if (badgeLevel >= options.low)
        classs = "alert-danger";

return new Ember.Handlebars.SafeString('<span class="badge pull-right">' + badgeLevel + '</span>');
});


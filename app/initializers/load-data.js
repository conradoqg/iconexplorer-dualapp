export default {
    name: "loadData",
    after: "store",

    initialize: function (container, application) {
        var store = container.lookup('store:main');

        store.findAll('config').then(function (all) {
            if (all.get('length') === 0) {
                var defaultConfig = store.createRecord('config', {
                    id: 'default',
                    size: 60,
                    color: '000000',
                    path: 'images',
                    showFavoritePackPopover: true,
                    showFavoriteIconPopover: true
                });

                defaultConfig.save();
            }
        });
    }
};
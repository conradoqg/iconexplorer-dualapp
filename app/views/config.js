export default Ember.View.extend({
    didInsertElement: function () {
        var self = this;
        $('#color-picker').pickAColor({
            inlineDropdown: true
        });
        $("#slider").slider({
            orientation: "horizontal",
            range: "min",
            max: 599,
            value: this.get('controller.size'),
            slide: refreshSwatch,
            change: refreshSwatch
        });

        $("[data-hide]").on("click", function () {
            $(this).closest("." + $(this).attr("data-hide")).hide();
        });

        function refreshSwatch() {
            self.get('controller').send('updateSize', $("#slider").slider("value"));
        }

        $('.progress').hide();

        if (App.isNodeWebkit) {
            $('#selectDirectoryButton').click(function () {
                $('#selectDirectory').trigger('click');
            });
            $("#selectDirectory").on('change', function (evt) {
                var path = $(this).val();
                if (path === '') return;
                self.get('controller').send('selectPath', path);
                $(this).val('');
            });
        }
    }
});
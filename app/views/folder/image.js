export default Ember.View.extend({
    didInsertElement: function () {
        var self = this;
        $('#color-picker').pickAColor({
            inlineDropdown: true,
            autoSelect: true,
            allowBlank: false
        });
        $("#slider").slider({
            orientation: "horizontal",
            range: "min",
            max: 599,
            value: this.get('controller.size'),
            slide: refreshSwatch,
            change: refreshSwatch
        });

        $("[data-hide]").on("click", function () {
            $(this).closest("." + $(this).attr("data-hide")).hide();
        });

        document.getElementById('svg_image_id').onload = function () {
            self.get('controller').changeSVGSize();
            self.get('controller').changeSVGColor();
        };

        function refreshSwatch() {
            self.get('controller').send('updateSize', $("#slider").slider("value"));
        }

        if (App.isNodeWebkit) {
            $('#saveButton').click(function () {
                $('#saveFile').trigger('click');
            });
            $("#saveFile").on('change', function (evt) {
                var filename = $(this).val();
                if (filename === '') return;
                self.get('controller').send('saveSVG', filename);
                $(this).val('');
            });
        }
    },

    configChanged: function () {
        var config = this.get('controller.model.config');
        if (config.get('showFavoriteIconPopover')) {
            setTimeout(function () {
                $('.favorite-icon-popover').popover('show');
            }, 500);
            setTimeout(function () {
                $('.favorite-icon-popover').popover('hide');
            }, 5500);
            this.get('controller').dontShowFavoritePopoverAnymore();
        }
    }.observes('controller.model.config'),

    willDestroyElement: function() {
        $('.favorite-icon-popover').popover('hide');
    }
});
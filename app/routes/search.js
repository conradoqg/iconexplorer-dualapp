import folders from 'appkit/models/folder';

export default Ember.Route.extend({
    model: function (params) {
        var regExp = new RegExp(params.query, 'i');
        var allFiles = from(from(folders).selectMany("from($.f).where('@.test($.i)', @)", regExp)).orderBy("$.i").toArray();
        return Em.RSVP.hash({
            rawFiles: allFiles,
            files: allFiles,
            isFiltered: false,
            query: params.query,
            config: this.store.find('config', 'default'),
            searchTerm: ''
        });
    }
});
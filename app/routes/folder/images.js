import folders from 'appkit/models/folder';

export default Ember.Route.extend({
    model: function (params) {
        var rawFiles = from(from(folders).where("$.i == @", params.folder_id).first().f).orderBy("$.i").toArray();
        return Em.RSVP.hash({
            folder: from(folders).where("$.i == @", params.folder_id).first(),
            rawFiles: rawFiles,
            files: rawFiles,
            config: this.store.find('config', 'default'),
            isFiltered: false,
            isFavorite: this.store.find('favoritefolder', { folder: params.folder_id }).then(function (favoriteFolder) {
                return favoriteFolder.get('length') > 0;
            }),
            searchTerm: ''
        });
    },
    renderTemplate: function () {
        this.render('folder.images', { into: 'folders' });
    }
});
import folders from 'appkit/models/folder';

export default Ember.Route.extend({
    model: function (params) {
        var folder =  from(folders).where("$.i == @", params.folder_id).first();
        return Em.RSVP.hash({
            folder: folder,
            file: from(folder.f).where("$.i == @", params.file_id).orderBy("$.i").first(),
            config: this.store.find('config', 'default'),
            isFavorite: this.store.find('favoritefile', { file: params.file_id }).then(function (favoriteFile) {
                return favoriteFile.get('length') > 0;
            })
        });
    },

    setupController: function (controller, model) {
        controller.set('model', model);
        if (controller.get('size') == null)
            controller.set('size', model.config.get('size'));
        if (controller.get('color') == null)
            controller.set('color', model.config.get('color'));
    },
    renderTemplate: function () {
        this.render('folder.image', { into: 'folders' });
    }
});
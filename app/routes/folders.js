import folders from 'appkit/models/folder';

export default Ember.Route.extend({
    model: function () {
        return {
            folders: folders,
            searchTerm: ''
        };
    }
});
import folders from 'appkit/models/folder';

export default Ember.Route.extend({
    model: function (params) {
        var rawFiles = this.getFavoriteFileIDS();
        var rawFolders = this.getFavoriteFolderIDS();
        return Em.RSVP.hash({
            rawFiles: rawFiles,
            files: rawFiles,
            rawFolders: rawFolders,
            folders: rawFolders,
            isFiltered: false,
            config: this.store.find('config', 'default'),
            searchTerm: ''
        });
    },

    getFavoriteFileIDS: function () {
        return this.store.find('favoritefile').then(function (favoriteFiles) {
            var idArray = [];
            favoriteFiles.forEach(function (favoriteFile) {
                idArray.push(favoriteFile.get('file'));
            });

            return from(from(folders).selectMany("$.f")).where("@.contains($.i.toString()) > 0",idArray).orderBy("$.i").toArray();
        });
    },
    getFavoriteFolderIDS: function () {
        return this.store.find('favoritefolder').then(function (favoriteFolders) {
            var idArray = [];
            favoriteFolders.forEach(function (favoriteFolder) {
                idArray.push(favoriteFolder.get('folder'));
            });
            return from(folders).where("@.contains($.i.toString()) > 0",idArray).orderBy("$.i").toArray();
        });
    }
});
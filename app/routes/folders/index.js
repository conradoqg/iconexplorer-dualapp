import folders from 'appkit/models/folder';

export default Ember.Route.extend({
    model: function (params) {
        return Em.RSVP.hash({
            folders: folders,
            fileCount: from(folders).select("$.f.length").sum(),
            favoriteFile: this.store.find('favoritefile'),
            favoriteFolder: this.store.find('favoritefolder'),
            config: this.store.find('config', 'default')
        });
    }
});
export default DS.Model.extend({
    size: DS.attr('number'),
    color: DS.attr('string'),
    path: DS.attr('string'),
    showFavoritePackPopover: DS.attr('boolean'),
    showFavoriteIconPopover: DS.attr('boolean')
});
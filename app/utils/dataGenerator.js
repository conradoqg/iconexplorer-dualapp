var generateData = function(baseDir, outDir, done, callback) {
    var dir = node_require('node-dir');
    var fs = node_require('fs');
    var path = node_require('path');
    var from = node_require('fromjs');

    var foldersData = [];

    var metadata = (JSON.parse(fs.readFileSync(path.resolve(baseDir, "metadata.json"), "utf8")));

    dir.subdirs(baseDir, function (err, subdirs) {
        if (subdirs) {
            var current = 0;
            var total = subdirs.length;
            subdirs.forEach(function (subdir) {
                dir.files(subdir, function (err, files) {
                    if (files) {
                        var folderItem = {
                            i: path.relative(baseDir, subdir),
                            f: []
                        };
                        var data = from(metadata).where("$.name == @", folderItem.i).single() ;
                        if (data) {
                            folderItem.sn = data.sourceName;
                            folderItem.dn = data.displayName;
                            folderItem.s = data.source;
                            folderItem.l = data.license;
                        }
                        var partialFilesData = [];
                        files.forEach(function (file) {
                            if (path.extname(file) === '.svg') {
                                var fileName = path.relative(subdir, file);
                                var fileItem = {
                                    i: path.basename(fileName, path.extname(fileName)),
                                    f: folderItem.i
                                };
                                partialFilesData.push(fileItem);
                            }
                        });
                        folderItem.f = from(partialFilesData).orderBy("$.i").toArray();
                        foldersData.push(folderItem);
                    }

                    current++;

                    if (callback) callback(total, current);

                    if (current === total) {
                        foldersData = from(foldersData).orderBy("$.i").toArray();

                        // Check if all found folders exist in metadata.json
                        console.log('Checking if all found folders exist in metadata.json...');
                        foldersData.forEach(function(folderData) {
                            if (!from(metadata).where("$.name == @", folderData.i).single())
                                console.log('folderData: ' + folderData.i + " not found in metadata");
                        })

                        // Check if all folders in metadata.json as found
                        console.log('Checking if all folders in metadata.json as found...');
                        metadata.forEach(function(data) {
                            if (!from(foldersData).where("$.i == @", data.name).single())
                                console.log('metadata: ' + data.name + " not found in foldersData");
                        })

                        // Check if file count in folder is equal to file count in metadata
                        console.log('Checkin if file count in folder is equal to file count in metadata...');
                        foldersData.forEach(function(folderData) {
                            var data = from(metadata).where("$.name == @", folderData.i).single() ;
                            if (data && folderData.f.length != parseInt(data.icons) && data.icons > folderData.f.length)
                                console.log('folderData: ' + folderData.i + " has " + folderData.f.length + " icons while metadata has " + data.icons);
                        })

                        console.log('Writing data.js...');
                        fs.writeFileSync(path.resolve(outDir, 'data.js'), 'function loadData() { return ' + JSON.stringify(foldersData) + ' }');
                        done();
                    }
                });
            });
        } else {
            done(false);
        }
    });
};

if (typeof module.exports !== 'undefined') module.exports.generateData = generateData;
var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

Router.map(function () {
	this.resource('folders', function () {
		this.resource('folder', function () {
			this.route('images', { path: ':folder_id' });
			this.route('image', { path: ':folder_id/:file_id' });
		});
	});
	this.resource('favorites');
	this.resource('search', { path: 'search/:query'});
	this.resource('config');
});

Router.reopen({
    notifyGoogleAnalytics: function() {
        return ((typeof window.ga !== 'undefined' && window.ga) ? window.ga('send', 'pageview', {
            'page': this.get('url'),
            'title': this.get('url')
        }) : true);
    }.on('didTransition')
});

export default Router;
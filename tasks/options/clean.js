module.exports = {
    'debug': ['tmp'],
    'dist': ['tmp', 'dist'],
    'nodewebkit': ['dist-nodewebkit']
};

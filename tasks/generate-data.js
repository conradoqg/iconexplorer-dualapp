module.exports = function(grunt) {
    grunt.registerTask('generateData', 'Generate data.', function () {
        var done = this.async();
        var dataGenerator = require('../app/utils/dataGenerator.js');
        var util = require('util');
        node_require = require;

        console.log('Generating data...');
        dataGenerator.generateData('data', 'data', done, function (total, current) {
            console.log('Current folder/total: ' + current + '/' + total);
        });
    });
};